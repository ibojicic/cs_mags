

def mass_dist_factor(final_mass):
    """ 
    CS mass distribution from Gesicki et al. 2014 (Fig 5)
    :param final_mass: CS final mass
    :return: float
    """
    if final_mass >= 0.53 and final_mass < 0.55:
        return 0.1
    elif final_mass >= 0.55 and final_mass < 0.57:
        return 0.25
    elif final_mass >= 0.57 and final_mass < 0.59:
        return 0.5
    elif final_mass >= 0.59 and final_mass < 0.61:
        return 0.1
    elif final_mass >= 0.61 and final_mass < 0.63:
        return 0.05
    else:
        return 0.


