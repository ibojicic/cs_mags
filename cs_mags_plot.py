import matplotlib
matplotlib.use('Agg')

import click
import matplotlib.pyplot as plt
import pandas as pd
import qb_ploting.templates as qbplottemps
import qb_common.selectors as qbselectors

@click.command()
@click.argument('hdf_file', nargs=1)

@click.option('--logteff_min', '-t', default=4.)
@click.option('--logl_min', '-l', default=3.)


def cli(hdf_file,logteff_min,logl_min):

    limits = [
        {'limits':[0.53,0.55],'value':0.1, 'group':1},
        {'limits':[0.55,0.57],'value':0.25, 'group':2},
        {'limits':[0.57,0.59],'value':0.5, 'group':3},
        {'limits':[0.59,0.61],'value':0.1, 'group':4},
        {'limits':[0.61,0.63],'value':0.05, 'group':5}
        ]

    ax_no = 0

    total_mags = []

    for lim in limits:
        plt.close('all')

        ax_no += 1
        ax_temp, ax_histo = qbplottemps.shared_x_1('M$_{Gaia\ G}$', 'Log T$_{eff}$', 'No', ax_no, [1, 3])
        track_mags_plot = []
        labels = []
        tracks_table = pd.read_hdf(hdf_file,'tracks',where = 'Finall_Mass >= {} and Finall_Mass <= {}'.format(lim['limits'][0],lim['limits'][1]),columns = ['group','Finall_Mass','set'])
        for tracks_index,tracks_row in tracks_table.iterrows():
            data_table = pd.read_hdf('cs_mags.h5', tracks_row['group'], where = 'LogL_sol > {} and Log_Teff > {}'.format(logl_min,logteff_min), columns=['t_in_track', 'Gaia_G','Log_Teff'])
            track_mags = []
            for index,row in data_table.iterrows():
                try:
                    no_mags = int(round(row['t_in_track'] * mass_dist_factor(tracks_row['Finall_Mass'])['value']))
                    track_mags = track_mags + no_mags * [row['Gaia_G']]
                except:
                    pass
            total_mags = total_mags + track_mags
            track_mags_plot.append(track_mags)
            labels = labels + [tracks_row['set']]
            ax_temp.plot(data_table.loc[:,'Gaia_G'], data_table.loc[:,'Log_Teff'])
        ax_histo.hist(track_mags_plot, normed=True, histtype='step',stacked=True,label = labels)
        ax_histo.legend(loc='upper left',frameon=False)

        plt.xlim(-6,6)
        plt.title('M {} - {}'.format(lim['limits'][0],lim['limits'][1]))
        plt.savefig('{}.eps'.format(ax_no))

    plot_axis = qbplottemps.basic_1('M$_{Gaia\ G}$','No',0)
    plot_axis.hist(total_mags,edgecolor='black', facecolor='lightgray', normed=True)
    plt.xlim(-6, 6)
    plt.savefig('test.eps')



def mass_dist_factor(final_mass):
    limits = [
        {'limits':[0.53,0.55],'value':0.1, 'group':1},
        {'limits':[0.55,0.57],'value':0.25, 'group':2},
        {'limits':[0.57,0.59],'value':0.5, 'group':3},
        {'limits':[0.59,0.61],'value':0.1, 'group':4},
        {'limits':[0.61,0.63],'value':0.05, 'group':5}
        ]

    return qbselectors.limited_fcn(limits,final_mass)
