from setuptools import setup

setup(
    name='CS magnitudes',
    author='Ivan Bojicic',
    author_email='ibojicic@gmail.com',
    license="BSD",

    version='0.1',
    py_modules=['cs_mags_plot', 'cs_mags_evolution'],
    install_requires=[
        # 'click',
    ],
    entry_points={'console_scripts': [
        'cs_mags_plot=cs_mags_plot:cli',
        'cs_mags_evolution=cs_mags_evolution:cli'
    ]}
)
