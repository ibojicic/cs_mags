import matplotlib

matplotlib.use('MacOSX')
import pyphot
from astropy import units as u
import os
import inspect
import click
import cs_functions as csf
from matplotlib import pyplot
import MySQLdb
import math
import qb_common.parse as qbparse
import numpy as np
from qb_ploting.templates import basic_1

@click.command()
@click.argument('band_filter', nargs=1)
@click.argument('distance', nargs=1, default=10.)
@click.argument('a_v', nargs=1, default=0.)
@click.option('--per_track', '-t', is_flag=True)
@click.option('--mass_dist', '-m', is_flag=True)
@click.option('--limit_mag', '-l', default = 7.)


def cli(band_filter, distance, a_v, per_track, mass_dist, limit_mag):

    ini_file = '{}/{}'.format(os.path.dirname(inspect.stack()[0][1]),'system.ini')
    mysql_ini = qbparse.parse_ini_arguments(ini_file,'mysqldb')
    database = MySQLdb.connect(**mysql_ini)

    cursor = database.cursor(cursorclass=MySQLdb.cursors.DictCursor)

    units = pyphot.ezunits.pint.UnitRegistry()
    angstrom = 1. * units.AA
    lib = pyphot.get_library()
    band_lib = lib[band_filter]

    lambdas = band_lib.wavelength
    if lambdas.units != angstrom.units:
        lambdas_AA = lambdas.to(units.AA).magnitude * u.AA
    else:
        lambdas_AA = lambdas.magnitude * u.AA

    mags = []

    valid_track = 0.

    for set in range(1, 61):
        print 'working on set {}'.format(set)
        sql = 'SELECT * FROM `ibojicic`.`Bertolami_2016_tracks` ' \
              'WHERE `Finall_Mass` >= 0.53 AND `Finall_Mass` <= 0.63 ' \
              'AND (`t_yr` > 0 AND `t_yr` < 20000)' \
              'AND `set` = {};'.format(set)
        cursor.execute(sql)
        tracks = cursor.fetchall()
        track_mags = {}
        if tracks:
            valid_track += 1.
            for key, position in enumerate(tracks):
                if mass_dist:
                    mass_factor = csf.mass_dist_factor(position['Finall_Mass'])
                else:
                    mass_factor = 1.

                t_eff = math.pow(10., position['Log_Teff'])

                l_solar = position['LogL_sol']
                t_in_track = position['t_in_track']
                final_mass = str(round(position['Finall_Mass'],2))
                try:
                    calc_mag = csf.calc_mags(lambdas_AA, lambdas, t_eff, l_solar, distance, a_v, band_lib)
                    no_objects = int(math.floor(t_in_track * mass_factor))

                    if calc_mag < limit_mag:
                        magnitudes = [calc_mag] * no_objects
                        mags = mags + magnitudes
                        t_eff_grp = group_by_temp(position['Log_Teff'])
                        if t_eff_grp not in track_mags:
                            track_mags[t_eff_grp] = []

                        track_mags[t_eff_grp] = track_mags[t_eff_grp] + magnitudes
                except:
                    print 'done set {}'.format(set)
            if per_track:
                set_lists = [track_mags[i] for i in track_mags]
                pyplot.hist(set_lists,histtype='barstacked',alpha=0.5)
                pyplot.savefig('{}_track_{}_Msol_histo.eps'.format(set, final_mass))
                pyplot.close()

                # pyplot.show()`
                # exit()

    hist, edges = np.histogram(mags, bins='auto',normed=True)
    w = abs(edges[1] - edges[0])
    bincenters = [edges[i] + w / 2. for i in range(len(edges) - 1)]
    pyplot.bar(bincenters,hist,width=w)
    pyplot.savefig('full_histo.eps')
    # pyplot.show()


def plot_hist(plt,mags,teff):
    track_hist, track_edges = np.histogram(track_mags, bins='auto', normed=True)
    w = abs(track_edges[1] - track_edges[0])
    track_bincenters = [track_edges[i] + w / 2. for i in range(len(track_edges) - 1)]
    pyplot.bar(track_bincenters, track_hist, width=w)
    pyplot.savefig('{}_track_{}_Msol_histo.eps'.format(set, final_mass))
    pyplot.close()


def group_by_temp(temp):
    if temp < 4.:
        return 1
    elif temp < 4.5:
        return 2
    elif temp < 5:
        return 3
    else:
        return 4

    # use_libs = ['HST_WFC3_F164N', 'CFHT_MEGAPRIME_CFH9702', 'CFHT_MEGAPRIME_CFH9701', 'HST_WFPC2_F791W', 'HST_WFC3_F621M', 'HST_WFC3_F125W', 'HST_ACS_HRC_F550M', 'HST_ACS_WFC_F775W', 'HST_ACS_WFC_F606W', 'JWST_NIRCAM_F140M', 'HST_NIC3_F113N', 'HST_WFC3_F845M', 'HST_WFC3_F680N', 'HST_WFC3_F139M', 'Gaia_RP', 'HST_ACS_HRC_F475W', 'CFHT_WIRCAM_CFH8305', 'CFHT_WIRCAM_CFH8304', 'HST_WFC3_F850LP', 'HST_WFC3_F390W', 'CFHT_WIRCAM_CFH8301', 'CFHT_WIRCAM_CFH8303', 'CFHT_WIRCAM_CFH8302', 'HST_WFC3_F390M', 'HST_ACS_HRC_F814W', 'WISE_RSR_W4', 'WISE_RSR_W1', 'SDSS_i', 'WISE_RSR_W3', 'HST_WFC3_F645N', 'HST_WFC3_F275W', 'HST_WFC3_F130N', 'JWST_NIRCAM_F410M', 'HST_NIC3_F160W', 'JWST_NIRCAM_F212N', 'CFHT_MEGAPRIME_CFH7803', 'HST_WFC3_F631N', 'JWST_NIRCAM_F070W', 'HST_ACS_WFC_F435W', 'HST_WFC3_F673N', 'HST_WFC3_F098M', 'HST_WFC3_FQ232N', 'HERSCHEL_PACS_GREEN', 'HST_WFC3_F502N', 'Gaia_BP', 'JWST_NIRCAM_F090W', 'CFHT_MEGAPRIME_CFH7701', 'JWST_NIRCAM_F323N', 'HST_NIC2_F205W', 'SDSS_r', 'JWST_NIRCAM_F162M', 'HST_ACS_HRC_F625W', 'HST_WFC3_F763M', 'GROUND_BESSELL_K', 'GROUND_BESSELL_J', 'GROUND_BESSELL_H', 'HST_WFC3_FQ672N', 'HST_WFPC2_F300W', 'HST_WFC3_F336W', 'HST_ACS_WFC_F555W', 'HST_WFC3_F953N', 'HST_ACS_WFC_F660N', 'HST_WFC3_FQ422M', 'HST_WFC3_F658N', 'HST_WFC3_F200LP', 'HST_ACS_HRC_F892N', 'JWST_NIRCAM_F277W', 'HERSCHEL_PACS_BLUE', 'HST_WFC3_F127M', 'HST_NIC2_F110W', 'JWST_NIRCAM_F164N', 'HST_NIC3_F164N', 'CFHT_WIRCAM_CFH8002', 'HST_WFC3_FQ387N', 'HST_WFC3_F110W', 'JWST_NIRCAM_F200W', 'CFHT_CFH12K_CFH7504', 'HST_NIC3_F205M', 'JWST_NIRCAM_F300M', 'JWST_NIRCAM_F460M', 'HST_WFC3_F657N', 'CFHT_MEGAPRIME_CFH9601', 'HST_WFC3_F218W', 'HST_WFC3_F140W', 'HST_WFC3_F167N', 'JWST_NIRCAM_F405N', 'HST_NIC3_F222M', 'HST_WFPC2_F450W', 'HST_WFPC2_F606W', 'GALEX_NUV', 'HST_WFPC2_F336W', 'HST_NIC2_F160W', 'HST_ACS_HRC_F220W', 'CFHT_WIRCAM_CFH8104', 'CFHT_WIRCAM_CFH8103', 'CFHT_WIRCAM_CFH8102', 'CFHT_WIRCAM_CFH8101', 'JWST_NIRCAM_F150W2', 'HST_WFC3_F775W', 'HST_NIC3_F212N', 'HERSCHEL_SPIRE_PSW', 'JWST_NIRCAM_F187N', 'HST_ACS_HRC_F660N', 'HST_WFC3_F689M', 'HST_WFC3_F350LP', 'HST_WFC3_FQ889N', 'JWST_NIRCAM_F466N', 'JWST_NIRCAM_F444W', 'HST_NIC3_F187N', 'HST_WFC3_FQ436N', 'HST_WFC3_F225W', 'HST_WFC3_F625W', 'HST_WFC3_F410M', 'HST_ACS_HRC_F606W', 'HST_WFC3_F160W', 'HST_WFC3_F600LP', 'Gaia_rvs', 'JWST_NIRCAM_F150W', 'HST_WFC3_F105W', 'HST_WFC3_F467M', 'HST_WFPC2_F814W', 'HST_WFC3_FQ575N', 'CFHT_CFH12K_CFH7406', 'HST_WFC3_FQ492N', 'HST_NIC3_F166N', 'HST_WFC3_FQ906N', 'HST_NIC3_F240M', 'HST_WFC3_F128N', 'JWST_NIRCAM_F322W2', 'HERSCHEL_SPIRE_PLW', 'HST_ACS_HRC_F775W', 'HST_NIC3_F190N', 'CFHT_MEGAPRIME_CFH7605', 'HST_WFC3_F814W', 'HST_ACS_WFC_F850LP', 'HST_ACS_HRC_F344N', 'HST_WFC3_F475X', 'CFHT_MEGAPRIME_CFH9301', 'SPITZER_IRAC_36', 'HST_ACS_WFC_F502N', 'JWST_NIRCAM_F335M', 'GROUND_COUSINS_R', 'HST_NIC3_F196N', 'HST_WFC3_F475W', 'HST_ACS_WFC_F814W', 'GROUND_COUSINS_I', 'HST_ACS_HRC_F250W', 'HST_WFC3_F665N', 'SDSS_z', 'JWST_NIRCAM_F360M', 'HST_WFC3_F126N', 'HST_WFC3_FQ243N', 'HST_ACS_WFC_F658N', 'HST_WFC3_F280N', 'HST_WFPC2_F255W', 'HERSCHEL_SPIRE_PLW_EXT', 'GALEX_FUV', 'HST_WFC3_F132N', 'SPITZER_IRAC_45', 'HST_ACS_HRC_F658N', 'HST_NIC3_F108N', 'JWST_NIRCAM_F115W', 'HERSCHEL_SPIRE_PMW', 'JWST_NIRCAM_F480M', 'HST_WFC3_F469N', 'WISE_RSR_W2', 'HST_WFC3_F555W', 'HST_WFC3_F656N', 'HST_WFC3_FQ378N', 'HST_WFC3_F487N', 'HST_WFPC2_F439W', 'HST_WFPC2_F218W', 'JWST_NIRCAM_F356W', 'SPITZER_IRAC_80', 'HST_WFC3_F606W', 'JWST_NIRCAM_F210M', 'CFHT_MEGAPRIME_CFH9401', 'HST_WFC3_FQ750N', 'HERSCHEL_PACS_RED', 'HST_WFC3_FQ937N', 'HST_WFPC2_F622W', 'HST_ACS_HRC_F330W', 'HST_WFC3_F373N', 'HST_WFPC2_F675W', 'HST_WFC3_F153M', 'HST_WFC3_FQ924N', 'CFHT_MEGAPRIME_CFH9801', 'HST_ACS_WFC_F550M', 'HST_WFC3_F343N', 'SDSS_g', 'Gaia_G', 'CFHT_WIRCAM_CFH8204', 'CFHT_WIRCAM_CFH8201', 'CFHT_WIRCAM_CFH8202', 'CFHT_WIRCAM_CFH8203', 'HST_ACS_WFC_F475W', 'HST_NIC3_F215N', 'HST_WFPC2_F170W', 'HST_WFC3_FQ619N', 'SPITZER_IRAC_58', 'HST_ACS_HRC_F850LP', 'HST_WFPC2_F850LP', 'JWST_NIRCAM_F182M', 'HST_WFC3_FQ437N', 'HST_NIC3_F110W', 'GROUND_JOHNSON_', 'GROUND_JOHNSON_V', 'HERSCHEL_SPIRE_PSW_EXT', 'HST_WFC3_FQ634N', 'HST_WFC3_F300X', 'HST_ACS_HRC_F435W', 'GROUND_JOHNSON_B', 'HST_ACS_WFC_F892N', '2MASS_H', '2MASS_J', 'JWST_NIRCAM_F430M', 'HST_WFC3_F438W', 'HST_WFC3_FQ508N', 'HST_WFC3_FQ727N', 'HST_WFC3_F547M', 'HST_NIC3_F200N', 'HST_WFC3_F395N', 'JWST_NIRCAM_F250M', 'HST_NIC3_F175W', 'HST_NIC3_F150W', 'HST_ACS_WFC_F625W', '2MASS_Ks', 'HST_ACS_HRC_F555W', 'HST_WFC3_FQ674N', 'JWST_NIRCAM_F470N', 'HST_WFPC2_F555W', 'HST_ACS_HRC_F502N', 'SDSS_']
