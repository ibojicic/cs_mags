import pyphot
import qb_common.mysqls as qbmysqls
import pandas as pd
import qb_astro.physics as myph
import numpy as np
from astropy import units as u
import tables

def cli():

    use_libs = [
        # 'WISE_RSR_W1',
        # 'WISE_RSR_W2',
        # 'WISE_RSR_W3',
        # 'WISE_RSR_W4',
        # 'SDSS_u',
        # 'SDSS_g',
        # 'SDSS_r',
        # 'SDSS_i',
        # 'SDSS_z',
        # 'GROUND_BESSELL_K',
        # 'GROUND_BESSELL_H',
        # 'GROUND_BESSELL_J',
        # 'GROUND_COUSINS_R',
        # 'GROUND_COUSINS_I',
        # 'GALEX_NUV',
        # 'GALEX_FUV',
        # 'SPITZER_IRAC_36',
        # 'SPITZER_IRAC_45',
        # 'SPITZER_IRAC_58',
        # 'SPITZER_IRAC_80',
        'Gaia_G',
        # 'GROUND_JOHNSON_U',
        # 'GROUND_JOHNSON_B',
        # 'GROUND_JOHNSON_V',
        # '2MASS_H',
        # '2MASS_J',
        # '2MASS_Ks',
        ]

    distance = 10.
    a_v = 0.

    # ini_file = '{}/{}'.format(os.path.dirname(inspect.stack()[0][1]), 'system.ini')
    database = qbmysqls.hash_database()

    libraries = prepare_libs(use_libs)

    store  = pd.HDFStore('cs_mags.h5','w')

    tracks_table = None

    for set in range(1, 61):
        sql = 'SELECT * FROM `ibojicic`.`Bertolami_2016_tracks` WHERE `set` = {};'.format(set)
        table = pd.read_sql(sql, database)
        if not table.empty:
            for libname in libraries:
                print "Working on {} set {}".format(libname,set)
                calc_mags(table, distance, a_v, libname, **libraries[libname])
                curr_group = "data/set_{}".format(set)
                set_data = set_columns(table,curr_group)
                store.put(curr_group,table,format='table',data_columns = True)

            if tracks_table is None:
                tracks_table = pd.DataFrame.from_dict([set_data])
            else:
                tracks_table = tracks_table.append([set_data])

    store.put('tracks',tracks_table,format='table',data_columns = True)
    store.flush()


def calc_mags(table,distance, a_v,libname, lambdas_AA,band_lib, lambdas):
    for index, row in table.iterrows():
        monochromatic_flux = myph.bb_monochr_flux(pow(10., row['Log_Teff']) * u.K, pow(10., row.loc['LogL_sol']),
                                                  lambdas_AA, distance, a_v)
        integrated_flux = band_lib.get_flux(lambdas, monochromatic_flux, axis=0)
        table.loc[index, libname] = -2.5 * np.log10(integrated_flux.value) - band_lib.Vega_zero_mag


def prepare_libs(selected_libs = []):
    units = pyphot.ezunits.pint.UnitRegistry()
    angstrom = 1. * units.AA
    lib = pyphot.get_library()
    all_libraries = lib.content
    result = {}
    for libname in all_libraries:
        if libname not in selected_libs:
            continue
        band_lib = lib[libname]

        lambdas = band_lib.wavelength
        if lambdas.units != angstrom.units:
            lambdas_AA = lambdas.to(units.AA).magnitude * u.AA
        else:
            lambdas_AA = lambdas.magnitude * u.AA

        result[libname] = {
            'lambdas_AA':lambdas_AA,
            'band_lib':band_lib,
            'lambdas':lambdas
        }

    return result

def set_columns(table,group_name):
    result_columns = {}
    return_columns = ['Initial_Mass','Finall_Mass','H','He','C','N','O','set']
    for clmn in return_columns:
        result_columns[clmn] = table.loc[0,clmn]
        del table[clmn]
    result_columns['group'] = group_name
    return result_columns

# curr_table = getattr(h5file.root, curr_group)
# curr_table._v_attrs.units = {'unit1': u.mag, 'unit2': u.Decibel}
# h5file.flush()
