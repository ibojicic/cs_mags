import pyphot
from astropy import units as u
import click
import cs_functions as csf

@click.command()
@click.argument('t_eff', nargs=1)
@click.argument('l_solar', nargs=1)
@click.argument('band_filter', nargs=1)
@click.argument('distance', nargs=1, default = 10.)
@click.argument('a_v', nargs=1, default = 0.)

@click.option('--filter_info', '-f', is_flag=True)



def cli(t_eff, l_solar, band_filter, distance, a_v, filter_info):
    units = pyphot.ezunits.pint.UnitRegistry()
    angstrom = 1. * units.AA
    lib = pyphot.get_library()
    band_lib = lib[band_filter]

    if filter_info:
        print band_lib.info()

    lambdas = band_lib.wavelength
    if lambdas.units != angstrom.units:
        lambdas_AA = lambdas.to(units.AA).magnitude * u.AA
    else:
        lambdas_AA = lambdas.magnitude * u.AA

    mag = csf.calc_mags(lambdas_AA, lambdas, t_eff, l_solar, distance, a_v, band_lib)
    print '{}:{}'.format(band_filter,mag)




